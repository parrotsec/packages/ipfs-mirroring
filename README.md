# Parrot Security OS - IPFS mirroring

The script automatically setup all to make a mirror of Parrot Security OS on IPFS

**YOU NEED TO INSTALL GO-IPFS FIRST! https://docs.ipfs.io/guides/guides/install/** but also **IPFS-CLUSTER** **https://cluster.ipfs.io/download/**

This tools is using the latest master branch of ipfs-cluster from github, as the crdt consensus algo is implemented here. So you will need to compile it from sources, as described in the linked guide. When the consensus algo will be released with the next release of ipfs-cluster, this reminder will be deleted.

The IPFS mirrors are meant to be used with apt-transport-ipfs: https://github.com/JaquerEspeis/apt-transport-ipfs , which you can find updated on: https://nest.parrotsec.org/parrot-packages/apt-transport-ipfs/

You can setup an mirror as a Provider Mirror or as a Standard Mirror, with the corresponding named setup scripts. 

As Provider Mirror, it will take several hours to setup the mirror the first time, **It is strongly recommended** to use screen or similar tools to keep the session active and usable, surely to insert sudo password when needed.

As Standard Mirror, you will automatically join the cluster of 

If you don't have sudo on your system and you want to run the scripts as root, remove sudo from the commands of the scripts with:

`sed -e "s/sudo//g" -i *-setup.sh`

before running the corresponding setup script.

The installers will automatically setup an **ipfs-parrot** user, with no login capabilities at all, so nologin shell and password disabled. For this reason, you can't use sudo to run and setup all directly with this user, sudo will refuse to impersonate the ipfs-parrot user. A group with the same name is created. 

A systemd script is also installed to manage the ipfs instance as a daemon. So actually, this systemd service works under Parrot Security OS up to 4.7 and all Debian/Ubuntu/OpenSUSE/CentOS/Fedora and other systemd distros and derivatives.

If necessary, when discovered that ipfs reaches the nofile limit (the number of opened files limit), as superuser (root) add this before the **"# End of file"** line, under /etc/security/limits.conf, like this:

```
...other file content...

ipfs-parrot             hard    nofile          4096
ipfs-parrot             soft    nofile          4096

# End of file
```

and also decomment:

`#export IPFS_FD_MAX=4096`

under the corresponding setup script, and:

`#Environment="IPFS_FD_MAX=4096"`

under the ipfs-parrot-mirror.service, before running the setup script.

TODO: make a service for other init systems too.

**<u>BE AWARE:</u>**

The files and the directories on the IPFS network are hashed. This means that, two files with the same name, added directly on the network from different nodes, are different files. 

Really, if they are the same file with the same content, the uploaded file should have the same hash in every case. But, when you are using this scripts, you are wrapping all the repo in a dir. The dirs of two Provider Mirrors should have the same hash for this reason, but, small changes on the files on the dir (like rsyncing in different moments also from the same mirror), make high probabilities to obtain different hashes for the final dirs.

Moreover, you need to provide the IPNS address to the users to point to your dir hash, so they can point to it in their APT sources lists, and provide the id of the trusted peer to the follow peers of your ipfs-cluster cluster.

You can obtain the info of your ipfs-cluster node with:

`ipfs-cluster-ctl id`

You will always provide the same files with the same hash, but you can end with no having a full updated copy of the whole dataset/repo. You will still provide the files with the same hashes, however:

If you do not have any reason to make another Provider Mirror on the network, or if you don't know what you are doing, **<u>PLEASE MAKE A STANDARD MIRROR USING THE SCRIPT NAMED "mirror-standard-setup.sh"</u>**.

In this way, you will make an exact copy, and after an update of the official Parrot Security OS mirror under IPFS, so the users will point to the same files across the whole network.

The environment variables are:

- WD to setup the Working Directory. The default is /srv/parrotsec_mirror
- In the case of the Standard Mirror setup, MIRROR is the IPFS-CLUSTER trusted peer address from where you are bootstrapping
- In the case of the Provider Mirror setup, RSYNC_MIRROR is the rsync mirror from where you fetch the repository from Internet

You can set them before launching the script with, as:

`WD=[absolute path] MIRROR=[ipfs-cluster trusted peer id] ./mirror-standard-setup.sh`

or:

`WD=[absolute path] RSYNC_MIRROR=[url of the rsync mirror] ./mirror-standard-setup.sh`

The defaults are:

- ftp.halifax.rwth-aachen.de/parrotsec for the rsync mirror

- the official Parrot Security OS IPFS-CLUSTER trusted peer address:

  /dns4/trusted-ipfs.parrotsec.org/tcp/9096/ipfs/12D3KooWPNg17jELNQERFp6sHo2W3nKPSpdkhQuwENJxFXMGeYEo (**Currently, this domain name is not setup yet**)

The official Parrot Security OS IPNS is: 

/ipns/QmUhEJjvaYWbxnGUD2Y5UsVrNNZHqpftEsq2QhQaKBgnqZ

You can try to port-mapping tcp/4001 and tcp/9096 to avoid NAT related issues.

NOTICE: the cronjob for syncing the Provider Mirror is set to start daily (at 0 0 * * *, so midnight, as @daily is not standard for all distribution). If you want to execute it differently, change the timing of the cronjob running this command before launching the setup script:

`sed -e "s/0 0 * * */NEW TIMING/g" -i mirror-provider-setup.sh`

where NEW TIMING is the new timing in the cron format, of course.

All the files on this GIT repository are under GPLv3+ 