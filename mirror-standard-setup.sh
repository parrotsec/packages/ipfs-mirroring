#!/usr/bin/env bash

set -e

ipfs_binary=`command -v ipfs`

if [ "${ipfs_binary}" == "" ]; then
  echo "You must install ipfs first, see https://docs.ipfs.io/guides/guides/install/"
  exit 1
else
  echo "Good! ipfs is installed under ${ipfs_binary}"
fi

ipfs_cluster_service_binary=`command -v ipfs-cluster-service`

if [ "${ipfs_cluster_service_binary}" == "" ]; then
  echo "You must install ipfs-cluster, from the master branch of github, first (so from sources). See https://cluster.ipfs.io/download/"
  exit 1
else
  echo "Good! ipfs-cluster-service is installed under ${ipfs_cluster_service_binary}"
fi

flock_binary=`command -v flock`

if [ "${flock_binary}" == "" ]; then
  echo "You must install flock first!"
  exit 1
else
  echo "Good! flock is installed under ${flock_binary}"
fi

awk_binary=`command -v awk`

if [ "${awk_binary}" == "" ]; then
  echo "You must install awk first!"
  exit 1
else
  echo "Good! awk is installed under ${awk_binary}"
fi

if [ -z "$WD" ]; then
  sudo mkdir -p /srv/parrotsec_mirror
  WD=/srv/parrotsec_mirror
fi

if [ -z "$MIRROR" ]; then
  MIRROR="/ip4/192.168.1.2/tcp/9096/ipfs/12D3KooWPNg17jELNQERFp6sHo2W3nKPSpdkhQuwENJxFXMGeYEo"
fi

IPFS_PATH="$WD"/.ipfs
IPFS_CLUSTER_PATH="$WD"/.ipfs-cluster

# Put a symlink to ipfs executable under /usr/bin/ to be sure crontab will find it

sudo ln -s "$(command -v ipfs)" /usr/bin/

# Decomment after setting nofile hardlimit to 4096 if necessary
# Remember, set the variable on the systemd user service too

#export IPFS_FD_MAX=4096

# Creating the ipfs user and deploying the systemd user service for the daemon
# FIXME: create the service also for other init systems, and make usage of the
# appropriate one

#adduser --shell /usr/sbin/nologin --home "$WD" --disabled-login --disabled-password --gecos "IPFS Daemon user" ipfs-parrot # Debian/Ubuntu version
sudo useradd -M -U -s /usr/sbin/nologin -d "$WD" -c "IPFS Daemon user for ParrotSec IPFS archive" ipfs-parrot # More general version

# Init the ipfs repo with badgerds datastore

sudo ipfs init --profile=server,badgerds

# Setting some useful features to make ipfs work properly with such
# big dataset, as our mirror

sudo ipfs config Reprovider.Strategy pinned
sudo ipfs config --json Datastore.NoSync true
sudo ipfs config --json Experimental.ShardingEnabled true

# Setting up ipfs-cluster node

sudo ipfs-cluster-service init

# Setting up the services

sudo cp mirror-provider-sync.sh "$WD"/
sudo chown ipfs-parrot:ipfs-parrot -R "$WD"
sed -i -e "s#WorkingDirectory=IPFS_HOME#WorkingDirectory=$WD#g" *.service
sed -i -e "s#IPFS_PATH=PATH_TO_REPO#IPFS_PATH=$WD/.ipfs#g" *.service
sed -i -e "s#IPFS_CLUSTER_PATH=PATH_TO_CLUSTER#IPFS_CLUSTER_PATH=$WD/.ipfs-cluster#g" ipfs-cluster-parrot-mirror.service
sudo cp ipfs-parrot-mirror.service /lib/systemd/system/ipfs-parrot-mirror.service
sudo cp ipfs-cluster-parrot-mirror.service /lib/systemd/system/ipfs-cluster-parrot-mirror.service
sudo systemctl daemon-reload
sudo systemctl enable ipfs-parrot-mirror.service
sudo systemctl enable ipfs-cluster-parrot-mirror.service
sudo systemctl start ipfs-parrot-mirror.service

# Bootstrapping from the ipfs-cluster trusted peer

sudo ipfs-cluster-service daemon --bootstrap $MIRROR
sudo chown ipfs-parrot:ipfs-parrot -R "$WD"
sudo systemctl start ipfs-cluster-parrot-mirror.service

echo "All done"
