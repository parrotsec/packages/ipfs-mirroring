#!/usr/bin/env bash

set -e

ipfs_binary=`command -v ipfs`

if [ "${ipfs_binary}" == "" ]; then
  echo "You must install ipfs first, see https://docs.ipfs.io/guides/guides/install/"
  exit 1
else
  echo "Good! ipfs is installed under ${ipfs_binary}"
fi

flock_binary=`command -v flock`

if [ "${flock_binary}" == "" ]; then
  echo "You must install flock first!"
  exit 1
else
  echo "Good! flock is installed under ${flock_binary}"
fi

awk_binary=`command -v awk`

if [ "${awk_binary}" == "" ]; then
  echo "You must install awk first!"
  exit 1
else
  echo "Good! awk is installed under ${awk_binary}"
fi

if [ -z "$WD" ]; then
  sudo mkdir -p /srv/parrotsec_mirror
  WD=/srv/parrotsec_mirror
fi

if [ -z "$MIRROR" ]; then
  MIRROR="/ipns/QmUhEJjvaYWbxnGUD2Y5UsVrNNZHqpftEsq2QhQaKBgnqZ"
fi

IPFS_PATH="$WD"/.ipfs

# Put a symlink to ipfs executable under /usr/bin/ to be sure crontab will find it

sudo ln -s "$(command -v ipfs)" /usr/bin/

# Decomment after setting nofile hardlimit to 4096 if necessary
# Remember, set the variable on the systemd user service too

#export IPFS_FD_MAX=4096

# Creating the ipfs user and deploying the systemd user service for the daemon
# FIXME: create the service also for other init systems, and make usage of the
# appropriate one

#adduser --shell /usr/sbin/nologin --home "$WD" --disabled-login --disabled-password --gecos "IPFS Daemon user" ipfs-parrot # Debian/Ubuntu version
sudo useradd -M -U -s /usr/sbin/nologin -d "$WD" -c "IPFS Daemon user for ParrotSec IPFS archive" ipfs-parrot # More general version

# Init the ipfs repo with badgerds datastore

sudo ipfs init --profile=server,badgerds

# Setting some useful features to make ipfs work properly with such
# big dataset, as our mirror

sudo ipfs config Reprovider.Strategy pinned
sudo ipfs config --json Datastore.NoSync true
sudo ipfs config --json Experimental.ShardingEnabled true

sudo cp mirror-standard-sync.sh "$WD"/
sudo chown ipfs-parrot:ipfs-parrot -R "$WD"
sed -i -e "s#WorkingDirectory=IPFS_HOME#WorkingDirectory=$WD#g" ipfs-parrot-mirror.service
sed -i -e "s#IPFS_PATH=PATH_TO_REPO#IPFS_PATH=$WD/.ipfs#g" ipfs-parrot-mirror.service
sudo cp ipfs-parrot-mirror.service /lib/systemd/system/ipfs-parrot-mirror.service
sudo systemctl daemon-reload
sudo systemctl enable ipfs-parrot-mirror.service
sudo systemctl start ipfs-parrot-mirror.service

echo "Setup completed, start first sync of the repo. This can take several hours"

# Add/pinning ipfs mirror from the ipfs network on the local storage

hash="$(ipfs name resolve -r $MIRROR | awk -F /ipfs/ '{print $2}')"

ipfs pin add "$hash"
sudo echo "$hash" > "$WD"/.ipfs/currenthash.txt
sudo chown ipfs-parrot:ipfs-parrot "$WD"/.ipfs/currenthash.txt

# You will obtain the new ipns name, updated on each iteration of the script
# Share the ipns mirror name :D

echo "Sync to IPFS done, finalising setup..."

# Running the Garbage Collector, optional but strongly recommended

echo "Running the GC"
ipfs repo gc --quiet

# Setting up crontab for standard mirror

(crontab -l 2>/dev/null; echo "0 0 * * * flock -xn /tmp/parrot-repo-sync.lock -c 'WD=$WD MIRROR=$MIRROR IPFS_PATH=$WD/.ipfs $WD/mirror-provider-sync.sh'") | crontab -u ipfs-parrot -

echo "All done"
