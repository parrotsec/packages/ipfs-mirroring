#!/usr/bin/env bash

set -e

ipfs_binary=`command -v ipfs`

if [ "${ipfs_binary}" == "" ]; then
  echo "You must install ipfs first, see https://docs.ipfs.io/guides/guides/install/"
  exit 1
else
  echo "Good! ipfs is installed under ${ipfs_binary}"
fi

awk_binary=`command -v awk`

if [ "${awk_binary}" == "" ]; then
  echo "You must install awk first!"
  exit 1
else
  echo "Good! awk is installed under ${awk_binary}"
fi

if [ -z "$WD" ]; then
  WD=/srv/parrotsec_mirror
fi

if [ -z "$MIRROR" ]; then
  MIRROR="/ipns/QmUhEJjvaYWbxnGUD2Y5UsVrNNZHqpftEsq2QhQaKBgnqZ"
fi

IPFS_PATH="$WD"/.ipfs

if [ ! -d "$WD"/.ipfs ]; then
  echo "You have to run ./mirror-standard-setup.sh first!"
  exit 1
fi

# Add/pinning ipfs mirror from the ipfs network on the local storage

hash="$(ipfs name resolve -r $MIRROR | awk -F /ipfs/ '{print $2}')"

if [ ! -f "$WD"/.ipfs/currenthash.txt ]; then
  ipfs pin add "$hash"
  echo "$hash" > "$WD"/.ipfs/currenthash.txt
else
  ipfs --api /ip4/127.0.0.1/tcp/9095 pin update "$(cat $WD/.ipfs/currenthash.txt)" "$hash"
  echo "$hash" > "$WD"/.ipfs/currenthash.txt
fi

# You will obtain the new ipns name, updated on each iteration of the script
# Share the ipns mirror name :D

echo "Sync to IPFS done"

# Running the Garbage Collector, optional but strongly recommended

echo "Running the GC"
ipfs repo gc --quiet
echo "All done"
