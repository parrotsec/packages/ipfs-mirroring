#!/usr/bin/env bash

set -e

ipfs_binary=`command -v ipfs`

if [ "${ipfs_binary}" == "" ]; then
  echo "You must install ipfs first, see https://docs.ipfs.io/guides/guides/install/"
  exit 1
else
  echo "Good! ipfs is installed under ${ipfs_binary}"
fi

ipfs_cluster_service_binary=`command -v ipfs-cluster-service`

if [ "${ipfs_cluster_service_binary}" == "" ]; then
  echo "You must install ipfs-cluster, from the master branch of github, first (so from sources). See https://cluster.ipfs.io/download/"
  exit 1
else
  echo "Good! ipfs-cluster-service is installed under ${ipfs_cluster_service_binary}"
fi

if [ -z "$WD" ]; then
  WD=/srv/parrotsec_mirror
fi

IPFS_PATH="$WD"/.ipfs
IPFS_CLUSTER_PATH="$WD"/.ipfs-cluster

if [ ! -d "$WD"/.ipfs ]; then
  echo "You have to run ./mirror-provider-setup.sh first!"
  exit 1
fi

if [ ! -d "$WD"/.ipfs-cluster ]; then
  echo "You have to run ./mirror-provider-setup.sh first!"
  exit 1
fi

# Add/update files to ipfs, not replicating them twice, using filestore
# recursive, wrapping in a directory, using filestore adding files
# returning the hash of the dir.

hash="$(ipfs add -r -w --raw-leaves --quieter "$WD"/parrotsec | tail -n1)"

# Publish/update the ipns name pointing to the latest hash

echo "Your ipns name data are these:"
ipfs name publish "$hash"

# You will obtain the new ipns name, updated on each iteration of the script
# Share the ipns mirror name :D

echo "Sync to IPFS done"

# Update pinning on all the cluster

if [ -f "$WD"/.ipfs/currenthash.txt ]; then
  ipfs --api /ip4/127.0.0.1/tcp/9095 pin update "$(cat $WD/.ipfs/currenthash.txt)" "$hash"
  echo "$hash" > "$WD"/.ipfs/currenthash.txt
else
  echo "$hash" > "$WD"/.ipfs/currenthash.txt
fi

# Running the Garbage Collector on all the cluster, optional but strongly recommended

echo "Running the GC"
ipfs --api /ip4/127.0.0.1/tcp/9095 repo gc
echo "All done"
